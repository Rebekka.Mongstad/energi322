%***********************************************************************************************
% Inelastic Catenary equations solver
% Developed by Dr. Bruno Roccia
%       Bergen Offshore Wind Centre, University of Bergen, Norway (2024)
% Contact: bruno.roccia@uib.no
%***********************************************************************************************

clear all
close all
clc

% This Matlab script solves the inelastic catenary equations detailed in page 7.41 of
% the notes developed by Dr. Finn Gunnar Nielsen

% DATA.m  : Mass per unit length of the cable
% DATA.g  : Gravity acceleration 
% DATA.H0 : Z-coordinate of the anchoring at X = 0 (negative for underwater applications)
% DATA.L  : Length of the cable
% DATA.XB : X-coordinate of the fair lead (or support) at X = XB

% DATA.Iter : Maximum number of iterations for the Newton-Raphson (N-R) method
% DATA.Tol  : Tolerance to stop the N-R method

% DATA.InitialGhess_H : Initial guess for the horizontal force H at X = XB
% DATA.InitialGhess_V : Initial guess for the vertical force V at X = XB

%% INPUT DATA

DATA.m  = 0.005097;
DATA.g  = 9.81;
DATA.H0 = 0;
DATA.L  = 2420;
DATA.XB = 2000;

DATA.Iter = 200;
DATA.Tol  = 1e-9;

DATA.InitialGhess_H = 50;
DATA.InitialGhess_V = 55;

%% 

Error = 1.0;
XOLD = [DATA.InitialGhess_H; DATA.InitialGhess_V];
k = 1;

while Error > DATA.Tol && (k < DATA.Iter)

    Res = Residue (DATA, XOLD);
    JJ  = Jacobian (DATA, XOLD);
    DX = JJ \ -Res;

    Error = norm(DX,inf);
    XOLD = XOLD + DX;
    k = k + 1;

end

%% REPORT 

fprintf('---------------------------------------------\n')
fprintf('Results Report\n')
fprintf('---------------------------------------------\n')
fprintf('Horizontal force at the fair lead: %12.8f [N] \n', XOLD(1,1))
fprintf('Vertical force at the fair lead: %12.8f [N] \n', XOLD(2,1))
fprintf('Error: %14.12f \n', Error)
fprintf('Number of iterations: %3i \n', k)

%%
H = XOLD(1,1);
V = XOLD(2,1);
L = DATA.L;

S = 0:01:DATA.L;
mg = DATA.m*DATA.g;
X = H/mg*(asinh((V-mg*(L-S))/H) - asinh((V-mg*L)/H));
Z = H/mg * (sqrt(1 + ((V-mg*(L-S))/H).^2 ) - sqrt(1 + ((V-mg*L)/H).^2));

figure(1)
plot(X,Z)
xlabel('x-direction [cm]')
ylabel('z-direction [cm]')
title('Inelastic cable configuration')
grid on

%%
T = sqrt(H^2 +(V-mg*(L-S)).^2);

figure(2)
plot(X,T)
grid on
xlabel('x-direction [cm]')
ylabel('Tension force [N]')
title('Inelastic cable tension force')

%% Functions

function RES = Residue (DATA, X)

% Residue function: this function evaluates the catenary equations at X
% Inputs: data strucutre variable DATA and real variable X
% DATA: structure variable containing all the parameters of the problem under study
% X: array of dimension 2x1 containing the estimations of H and V
% Outputs: array variable RES of dimension 2x1 resulting from evaluating the catenary equations

mg  = DATA.m*DATA.g;
H0  = DATA.H0;
L   = DATA.L;
XB  = DATA.XB;
H   = X(1,1);
V   = X(2,1);

RES = zeros (2,1);

RES(1,1) = H/mg*(asinh(V/H) - asinh((V-mg*L)/H)) - XB;
RES(2,1) = H/mg * (sqrt(1 + ((V-mg*L)/H)^2 ) - sqrt(1 + (V/H)^2)) - H0;

end

function JJ = Jacobian (DATA, X)

% Jacobian function: this function evaluates the Jacobian matrix at X
% Inputs: data strucutre variable DATA and real variable X
% DATA: structure variable containing all the parameters of the problem under study
% X: array of dimension 2x1 containing the estimations of H and V
% Outputs: array variable JJ of dimension 2x2 resulting from evaluating the Jacobian at X

mg = DATA.m*DATA.g;
L   = DATA.L;
H   = X(1,1);
V   = X(2,1);

JJ = zeros (2,2);

JJ(1,1) = 0.1e1 / mg * (asinh(V / H) - asinh((-mg * L + V) / H)) + H / mg * (-V / H ^ 2 * (0.1e1 + 0.1e1 / H ^ 2 * V ^ 2) ^ (-0.1e1 / 0.2e1) + ...
          (-mg * L + V) / H ^ 2 * (0.1e1 + (-mg * L + V) ^ 2 / H ^ 2) ^ (-0.1e1 / 0.2e1));
JJ(1,2) = H / mg * (1 / H * (1 + 1 / H ^ 2 * V ^ 2) ^ (-0.1e1 / 0.2e1) - 1 / H * (1 + (-mg * L + V) ^ 2 / H ^ 2) ^ (-0.1e1 / 0.2e1));
JJ(2,1) = 0.1e1 / mg * (sqrt((1 + (-mg * L + V) ^ 2 / H ^ 2)) - sqrt((1 + 1 / H ^ 2 * V ^ 2))) + (H / mg * (-(1 + (-mg * L + V) ^ 2 / H ^ 2) ^ (-0.1e1 / 0.2e1) * ...
          (-mg * L + V) ^ 2 / H ^ 3 + (1 + 1 / H ^ 2 * V ^ 2) ^ (-0.1e1 / 0.2e1) / H ^ 3 * V ^ 2));
JJ(2,2) = H / mg * (-V / H ^ 2 * (1 + 1 / H ^ 2 * V ^ 2) ^ (-0.1e1 / 0.2e1) + (-mg * L + V) / H ^ 2 * (1 + (-mg * L + V) ^ 2 / H ^ 2) ^ (-0.1e1 / 0.2e1));

end