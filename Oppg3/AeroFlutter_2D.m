%***********************************************************************************************
% Two-dimensional flutter analysis
% Developed by Dr. Bruno Roccia
%       Bergen Offshore Wind Centre, University of Bergen, Norway (2024)
% Contact: bruno.roccia@uib.no
%***********************************************************************************************

clear all
close all
clc

% This Matlab script numerically integrates in the time domain the
% governing equations of a two-dimensional aeroelastic system. To
% integrate the system of ODEs, the script uses the integrator ode45

% Output variable: structure RESPONSE containing the following fields
%                  .Time (vector containing the time)
%                  .Plunge (vector containing plunge time-series solution evaluated at Time)
%                  .Pitching (vector containing pitching time-series solution evaluated at Time)
%                  .DPlunge (vector containing plunge velocity solution evaluated at Time)
%                  .DPitching (vector containing pitching angular velocity solution evaluated at Time)

%% DATA

DATA.C      = 18.288;               % Wing chord  
DATA.kh     = 9.9818e+3;            % Linear plunge spring stiffness
DATA.kt     = 1.614834e+6;          % Linear pitching spring stiffness
DATA.m      = 12879.8;              % Mass per unit length
DATA.Ip     = 6.7005e+5;            % Wing mss moment of inertia
DATA.dh     = 0;                    % Plunge damping coefficient
DATA.dt     = 0;                    % Pitching damping coefficient
DATA.khc    = 0;                    % Cubic plunge spring stiffness
DATA.ktc    = 1.5 * DATA.kt;        % Cubic pitching spring stiffness
DATA.xs     = 1.0;                  % Elastic axis dimensionless distance from the leading edge
DATA.xa     = 0;                    % Distance between the elastic axis and mass center
DATA.U      = 80;                 % Free-stream velocity
DATA.Rhoinf = 1.225;                % Air density

NUM.DT      = 0.01;                 % Time step
NUM.TFinal  = 20000;                % simulation time
NUM.CI      = [0 1*pi/180 0 0];     % Initial conditions

%% Dimensionless quantities

Wh = sqrt(DATA.kh/DATA.m);
Wt = sqrt(DATA.kt/DATA.Ip);

DATA.Xi_h   = DATA.dh / (2*sqrt(DATA.m*DATA.kh));          
DATA.Xi_a   = DATA.dt / (2*sqrt(DATA.m*DATA.kt));            
DATA.mu     = DATA.m / (pi*DATA.Rhoinf*(DATA.C/2)^2);         
DATA.ra     = sqrt(DATA.Ip/(DATA.m*(DATA.C/2)^2));        
DATA.ah     = DATA.xs - 1;        
DATA.xa     = DATA.xa;         
DATA.Omb    = Wh/Wt;         
DATA.Beta   = DATA.ktc / DATA.kt;           
DATA.Gamma  = DATA.khc / DATA.kh;         
DATA.Uinf   = DATA.U / (DATA.C/2*Wt);      

DATA.Psi1   = 0.165;
DATA.Psi2   = 0.335;
DATA.Eps1   = 0.0455;
DATA.Eps2   = 0.3;

%% Solution procedure

Tspan = [0 NUM.TFinal];
CInitial = [NUM.CI, 0, 0, 0, 0];

Term = Matrices (DATA);
[Tout, Zout] = ode45 (@Func, Tspan, CInitial, [], DATA, Term, NUM);
SOL.Time = Tout;
SOL.Z    = Zout;

RESPONSE.Time = SOL.Time * DATA.C/2 / DATA.U;
RESPONSE.Plunge = SOL.Z(:,1) * DATA.C/2;
RESPONSE.Pitching = SOL.Z(:,2) * 180 / pi;
RESPONSE.DPlunge = SOL.Z(:,3) * (DATA.C/2)^2 / DATA.U;
RESPONSE.DPitching = SOL.Z(:,4) * 180 / pi * DATA.C/2 / DATA.U;

%% PLOT

figure (1)
plot (RESPONSE.Time, RESPONSE.Plunge,"Color",'b')
hold on
plot(xlim,[0 0], 'color','k','linestyle','--')
title('Plunge oscillation')
xlabel('Time [s]')
ylabel('Plunge amplitud [m]')

figure(2)
plot (RESPONSE.Time, RESPONSE.Pitching,"Color",'b')
hold on
plot(xlim,[0 0], 'color','k','linestyle','--')
title('Pitching oscillation')
xlabel('Time [s]')
ylabel('Pitching amplitud [º]')

%% Functions

%%

figure (3)
plot (RESPONSE.DPlunge, RESPONSE.Plunge,"Color",'b')
hold on
plot(xlim,[0 0], 'color','k','linestyle','--')
title('LCO, Plunge ')


figure(4)
plot (RESPONSE.DPitching, RESPONSE.Pitching,"Color",'b')
hold on
plot(xlim,[0 0], 'color','k','linestyle','--')
title('LCO, Pitch')

%%
A_pitch=[0,0,0,0,0.92,9.49,16.2,21, 25.1,32, 46.4, 58.8 ]
A_plunge=[0,0,0,0,0.19,1.99,3.42,4.46,5.36, 6.9, 10.25, 13.12  ]
V=[10,20,30, 40,48.93,50,52,54,56, 60, 70, 80 ]

figure(5)
plot(V, A_pitch)
hold on
plot(V, A_pitch, 'x', 'MarkerFaceColor', 'red', 'MarkerSize', 9)
title('Bifurcation curv for Pitch')
xlabel('Amplitude')
ylabel('Pitching - LCO amplitud [º]')

figure(6)
plot(V, A_plunge)
hold on
plot(V, A_plunge, 'x', 'MarkerFaceColor', 'red', 'MarkerSize', 9)
title('Bifurcation curv for Plunge')
xlabel('Amplitude')
ylabel('Plunge - LCO amplitud [m]')


